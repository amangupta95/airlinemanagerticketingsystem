package com.amts.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.swing.text.html.FormSubmitEvent.MethodType;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.amts.dao.CityDao;
import com.amts.dao.FlightDao;
import com.amts.exception.CityException;
import com.amts.exception.FlightException;
import com.amts.pojo.City;
import com.amts.pojo.Flight;

@Controller
@RequestMapping("/flight/*")
public class FlightController {

	@RequestMapping(value="/displayFlights", method=RequestMethod.GET)
	public List<Flight> getFlights(HttpServletRequest request, FlightDao flightDao) throws ParseException, FlightException{
		String origin = request.getParameter("fromCity");
		String destination = request.getParameter("toCity");
		Date date = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("departDate"));
		List<Flight> flights = flightDao.getFlights(origin, destination, date);
		return flights;
	}
	
	@RequestMapping(value="/createFlights", method=RequestMethod.POST)
	public String createFlight(HttpServletRequest request, FlightDao flightDao) throws FlightException{
		
		return "createFlight";
	}
	
	@RequestMapping(value="/createFlight", method=RequestMethod.GET)
	public String createFlight(ModelMap modelMap, FlightDao flightDao, CityDao cityDao) throws FlightException, CityException{
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String currDate = format.format(date);
		modelMap.addAttribute("today", currDate);
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MONTH, 3);
		String maxDate = format.format(c.getTime());
		List<City> cities = cityDao.getCities();
		modelMap.addAttribute("maxDate", maxDate);
		modelMap.addAttribute("cities", cities);
		return "createFlight";
	}
}
