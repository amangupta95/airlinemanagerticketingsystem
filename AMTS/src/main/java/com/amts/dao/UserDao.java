package com.amts.dao;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

import com.amts.exception.UserException;
import com.amts.pojo.User;

public class UserDao extends Dao {
	
	public UserDao() {
		
	}
	
	public int register(User u) throws UserException{
		try {
			begin();
			int i = (Integer)getSession().save(u);
			commit();
			return i;
		}catch(HibernateException ex) {
			rollback();
			throw new UserException("Could not save user: "+ ex.getMessage(),ex.getCause());
		}
	}
	
	public User get(User user) throws UserException {
		try {
			begin();
			Criteria crit = getSession().createCriteria(User.class);
			crit.add(Restrictions.eq("email", user.getEmail()));
			crit.add(Restrictions.eq("password", user.getPassword()));
			User u = (User)crit.uniqueResult();
			commit();
			return u;
		} catch (HibernateException e) {
			rollback();
			throw new UserException("Could not get user " + user.getEmail(), e);
		}
	}
	
	public User get(String email, String password) throws UserException {
		try {
			begin();
			Query hql = getSession().createQuery("from User where email=:email and password=:password");
			hql.setString("email", email);
			hql.setString("password", password);
			User u = (User)hql.uniqueResult();
			commit();
			return u;
		} catch(HibernateException e) {
			rollback();
			throw new UserException("Could not get user "+ email, e);
		}finally {
			close();
		}
	}

}
