package com.amts.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.springframework.util.ResourceUtils;

import com.amts.pojo.Aircraft;
import java.util.List;
import java.util.ArrayList;

public class AircraftFileParser {
	
	public List<Aircraft> readAndSaveAircrafts(String fileName) throws IOException{
		String csvSplitBy=",";
		List<Aircraft> aircrafts = new ArrayList<>();
		try
		{
			File file = ResourceUtils.getFile(fileName);
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line = br.readLine();
			while(line!=null) {
				String[] aircraft = line.split(csvSplitBy);
				Aircraft a = new Aircraft();
				a.setModel(aircraft[0]);
				a.setManufacturer(aircraft[1]);
				a.setCapacity(Integer.parseInt(aircraft[2]));
				a.setPrice(Double.parseDouble(aircraft[3]));
				aircrafts.add(a);
				line = br.readLine();
			}
			br.close();
			return aircrafts;
		}catch(IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}
}
