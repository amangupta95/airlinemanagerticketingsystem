<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h2>Welcome to AMTS</h2>
	<c:set var="contextPath" value="${pageContext.request.contextPath}" />
	<form:form action="${contextPath}/register" method="post" modelAttribute="user">
		<label>First Name:	</label><form:input path="firstName" required="required" placeholder="Enter your email" type="text" /><br/>
		<label>Last Name:   </label><form:input path="lastName" type="text" required="required" placeholder="Enter your email" /><br/>
		<label>Email:    	</label><form:input path="email" type="email" required="required" placeholder="Enter your email" /><br/>
		<label>Password: 	</label><form:input path="password" type="password" required="required" placeholder="Password" />
		<label>Phone:		</label><form:input path="phoneNumber" type="number" required="required" placeholder="Phone Number" />
		<input type="submit" value="Login" name="submit"/>
	</form:form>
	<a href="${contextPath}/register/airline">Register as an Airline</a>
</body>
</html>