<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
	<title>Home</title>
	<link rel="stylesheet" href="<c:url value="/resources/css/main.css" />" type="text/css" />
</head>
<body style="display: flex">
	<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
	<div style="width: 20%; background-color: powderblue; height: 562px; text-align: center">
		<ul id="adminMenu">
			<li><a href="${contextPath}/admin/dashboard">Dashboard</a></li>
			<li><a href="${contextPath}/aircraft/addAircraft">Aircrafts</a></li>
			<li><a href="${contextPath}/airline/approveAirline">Airlines</a></li>
		</ul>
	</div>
	<div style="width: 80%">
		<div id="dashboard">
			<table id="stats_table">
				<tr><td>Total aircrafts: </td><td>${aircrafts}</td></tr>
				<tr><td>Total Airlines approved: </td><td>${airlines_approved}</td></tr>
				<tr><td>Total Airlines pending: </td><td>${airlines_pending}</td></tr>
				<tr><td>Total Users registered: </td><td>${users}</td></tr>
			</table>
		</div>
	</div>
</body>
</html>
