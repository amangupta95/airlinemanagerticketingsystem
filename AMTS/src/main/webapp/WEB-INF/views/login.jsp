<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>AMTS-Login</title>
<link rel="stylesheet" href="<c:url value="/resources/css/main.css" />" type="text/css" />
</head>
<body>
	<h2>Welcome to AMTS</h2>
	<c:set var="contextPath" value="${pageContext.request.contextPath}" />
	<form action="${contextPath}/login" method="post">
		<label>Email:    </label><input name="email" type="email" required="required" placeholder="Enter your email" /><br/>
		<label>Password: </label><input name="password" type="password" required="required" placeholder="Password" /><br/>
		<label>Login As: </label><input name="role" type="radio" value="Customer" required="required">Customer
		<input name="role" type="radio" value="Airliner" required="required"/>Airline
		<input name="role" type="radio" value="Admin" required="required"/>Admin</br>
		<input type="submit" value="Login" name="submit"/>
	</form>
	<a href="${contextPath}/register/user">Register</a>
</body>
</html>