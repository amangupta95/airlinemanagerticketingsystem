<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>AMTS- Plan Your Flight</title>
</head>
<body>
	<h1>Welcome ${user}</h1>
	<c:set var="contextPath" value="${pageContext.request.contextPath}" />
	<form method="get" action="${contextPath}/flight/displayFlights">
		<label>From: </label>
		<select name="fromCity">
			<c:forEach items="${cities}" var="city">
				<option value="${city.iata}">
					<div style="display: flex">
						<div style="width: 70%">
							<p>${city.cityName}</p>,<p> ${city.country}</p></br>
							<p>${city.airport}</p>
						</div>
						<div style="width: 30%">${city.iata}</div>
					</div>
				</option>
			</c:forEach>
		</select></br>
		<label>To: </label>
		<select name="toCity">
			<c:forEach items="${cities}" var="city">
				<option value="${city.iata}">
					<div style="display: flex">
						<div style="width: 70%">
							<p>${city.cityName}</p>,<p> ${city.country}</p></br>
							<p>${city.airport}</p>
						</div>
						<div style="width: 30%">${city.iata}</div>
					</div>
				</option>
			</c:forEach>
		</select><br/>
		<label>Date: </label>
		<input type="date" name="departDate" min="${today}" max="${maxDate}"/><br/>
		<label>Passengers: </label>
		<input type="number" name="travellers" value=1 min=1 /><br/>
		<input type="submit" name="submit" value="Find Flights" />
	</form>
</body>
</html>