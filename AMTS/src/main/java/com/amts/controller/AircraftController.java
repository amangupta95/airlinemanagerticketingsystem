package com.amts.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.amts.dao.AircraftDao;
import com.amts.dao.AircraftOrderDao;
import com.amts.dao.AirlineDao;
import com.amts.exception.AircraftException;
import com.amts.exception.AircraftRequestException;
import com.amts.exception.AirlineException;
import com.amts.pojo.Aircraft;
import com.amts.pojo.AircraftRequest;
import com.amts.pojo.Airline;
import com.amts.util.AircraftFileParser;


@Controller
@RequestMapping("/aircraft/*")
public class AircraftController {
	
	@RequestMapping(value="/addAircraft", method=RequestMethod.GET)
	public String getAddAircraftView(ModelMap modelMap, AircraftOrderDao aircraftOrderDao, AirlineDao airlineDao, AircraftFileParser air) 
			throws AircraftRequestException, IOException, AirlineException{
		Aircraft aircraft = new Aircraft();
		List<AircraftRequest> requests = aircraftOrderDao.getOrders();
		List<Aircraft> aircrafts = air.readAndSaveAircrafts("classpath:Aircrafts.csv"); 
		Map<Integer, Aircraft> airplanes = new HashMap<>();
		for(AircraftRequest request: requests) {
			for(Aircraft a: aircrafts) {
				if(a.getModel().equals(request.getAircraft_model())) {
					a.setAirline(airlineDao.getAirline(request.getAirline_id()));
					a.setCount(request.getCount());
					airplanes.put(request.getId(),a);
				}
			}
		}
		modelMap.addAttribute(aircraft);
		modelMap.addAttribute("orderRequests",airplanes);
		return "aircraft";
	}
	
	@RequestMapping(value="/addAircraft", method=RequestMethod.POST)
	public String addAircraft(AircraftDao aircraftDao, @ModelAttribute("aircraft") Aircraft aircraft, HttpServletRequest request,
			ModelMap modelMap, AircraftOrderDao aircraftOrderDao, AircraftFileParser air, AirlineDao airlineDao) 
			throws AircraftException, AircraftRequestException, AirlineException, IOException {
		int id = Integer.parseInt(request.getParameter("airline_id"));
		int request_id = Integer.parseInt(request.getParameter("request_id"));
		Airline airline = airlineDao.getAirline(id);
		aircraft.setAirline(airline);
		airline.getAircrafts().add(aircraft);
		aircraftDao.add(aircraft, id);
		aircraftOrderDao.updateOrder(request_id);
		return this.getAddAircraftView(modelMap, aircraftOrderDao, airlineDao, air);
	}
	
	@RequestMapping(value="/orderAircraft", method=RequestMethod.GET)
	public String getOrderAircraftView(ModelMap modelMap, AircraftFileParser parse) throws IOException{
		List<Aircraft> aircrafts = parse.readAndSaveAircrafts("classpath:Aircrafts.csv");
		AircraftRequest aircraftRequest = new AircraftRequest();
		modelMap.addAttribute(aircraftRequest);
		modelMap.addAttribute("aircrafts",aircrafts);
		return "orderAircraft";
	}
	
	@RequestMapping(value="/orderAircraft", method=RequestMethod.POST)
	public String orderAircraft(AircraftOrderDao aircraftOrderDao, @ModelAttribute("request") AircraftRequest request, ModelMap modelMap, AircraftFileParser parse) throws IOException, AircraftRequestException{
		aircraftOrderDao.submitOrder(request);
		return this.getOrderAircraftView(modelMap, parse);
	}
}
