<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="<c:url value="/resources/css/main.css" />" type="text/css" />
</head>
<body style="display: flex">
	<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
	<c:set var="airlineId" value="${sessionScope.airlineId }"/>
	<div style="width: 20%; background-color: powderblue; height: 562px; text-align: center">
		<ul id="airlineMenu">
			<li><a href="${contextPath}/airline/dashboard">Dashboard</a></li>
			<li><a href="${contextPath}/aircraft/orderAircraft">Aircrafts</a></li>
			<li><a href="${contextPath}/flight/createFlight">Flights</a></li>
		</ul>
	</div>
	<div style="width: 80%">
		<div id="orderAircraft">
			<form:form action="${contextPath}/aircraft/orderAircraft" method="post" modelAttribute="aircraftRequest">
				<form:select name="aircraft" path="aircraft_model">
					<c:forEach items="${aircrafts}" var="item">
						<option value="${item.model}">${item.manufacturer} ${item.model}</option>
					</c:forEach>
				</form:select>
				<div>
					<form:input type="hidden" path="airline_id" value="${airlineId}"/>
					<form:input path="count" type="number" min = "0"/>
				</div>
				<input type="submit" name="submit" value="Order" />
			</form:form>
		</div>
	</div>
</body>
</html>