package com.amts.exception;

public class BookingException extends Exception {
	public BookingException(String message, Throwable cause) {
		super("Booking Exception: "+message, cause);
	}
	public BookingException(String message) {
		super("Booking Exception: "+message);
	}
}
