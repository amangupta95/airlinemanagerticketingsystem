package com.amts.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;

import com.amts.exception.CityException;
import com.amts.pojo.City;

public class CityDao extends Dao{
	
	public void populateCity(City city) throws CityException {
		try {
			begin();
			getSession().save(city);
			commit();
		}catch(HibernateException ex) {
			rollback();
			throw new CityException("could not save city: "+ city+ " "+ ex.getMessage(), ex.getCause());
		}
	}
	
	public long countCity() throws CityException {
		try {
			begin();
			Criteria crit = getSession().createCriteria(City.class);
			crit.setProjection(Projections.rowCount());
			long count = (Long)crit.uniqueResult();
			commit();
			return count;
		}catch(HibernateException ex) {
			rollback();
			throw new CityException(ex.getMessage(), ex.getCause());
		}finally {
			close();
		}
	}
	
	public List<City> getCities() throws CityException {
		try {
			begin();
			Query hql = getSession().createQuery("from City");
			List<City> cities = hql.list();
			commit();
			return cities;
		}catch(HibernateException ex) {
			rollback();
			throw new CityException(ex.getMessage(), ex.getCause());
		}finally {
			close();
		}
	}
	
}
