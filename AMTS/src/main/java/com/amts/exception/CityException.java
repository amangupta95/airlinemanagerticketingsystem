package com.amts.exception;

public class CityException extends Exception {
	public CityException(String message, Throwable cause) {
		super(message, cause);
	}
}
