package com.amts.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.amts.dao.AirlineDao;
import com.amts.dao.CityDao;
import com.amts.dao.UserDao;
import com.amts.exception.AirlineException;
import com.amts.exception.CityException;
import com.amts.exception.UserException;
import com.amts.pojo.User;
import com.amts.pojo.Airline;
import com.amts.pojo.City;

@Controller
public class AuthenticationController {
	
	@RequestMapping(value="/login", method=RequestMethod.POST)
	public ModelAndView loginUser(ModelMap modelMap, HttpServletRequest request, 
			UserDao userDao, AirlineDao airlineDao, CityDao cityDao) throws UserException, AirlineException, CityException {
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String role = request.getParameter("role");
		if(role.equals("Customer")) {
			User u = userDao.get(email, password);
			if(u!=null) {
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				Date date = new Date();
				String currDate = format.format(date);
				modelMap.addAttribute("today", currDate);
				Calendar c = Calendar.getInstance();
				c.add(Calendar.MONTH, 3);
				String maxDate = format.format(c.getTime());
				List<City> cities = cityDao.getCities();
				HttpSession session = request.getSession();
				session.setAttribute("userId", u.getId());
				modelMap.addAttribute("user", u.getFirstName()+" "+u.getLastName());
				modelMap.addAttribute("maxDate", maxDate);
				modelMap.addAttribute("cities", cities);
				return new ModelAndView("welcomePage", modelMap);
			}
		}else if(email.equals("admin@admin.com")&& password.equals("admin")&&role.equals("Admin")){
			return new ModelAndView("home");
		}else {
			Airline a = airlineDao.getAirline(email, password);
			if(a!=null) {
				HttpSession session = request.getSession();
				session.setAttribute("airlineId", a.getId());
				return new ModelAndView("dashboard", "airline",a);
			}
		} 
		return new ModelAndView("login");
	}
	
	@RequestMapping(value="/register/user", method=RequestMethod.POST)
	public String registerUser(HttpServletRequest request, @ModelAttribute("user") User user, UserDao userDao) throws UserException {
		int i = userDao.register(user);
		if(i>0)
			return "success";
		return "login";
	}
	
	@RequestMapping(value="/register/user", method=RequestMethod.GET)
	public String registerUser(ModelMap model, User u) throws UserException {
		model.addAttribute("user", u);
		return "register_user";
	}
	
	@RequestMapping(value="/register/airline", method=RequestMethod.POST)
	public String registerAirline(HttpServletRequest request, @ModelAttribute("airline") Airline airline, AirlineDao airlineDao) throws AirlineException {
		airline.setApproved(false);
		int i = airlineDao.register(airline);
		if(i>0)
			return "success";
		return "login";
	}
	
	@RequestMapping(value="/register/airline", method=RequestMethod.GET)
	public String registerAirline(ModelMap model, Airline airline){
		model.addAttribute("airline", airline);
		return "register_airline";
	}
}
