package com.amts.dao;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import com.amts.exception.FlightException;
import com.amts.pojo.DateValue;
import com.amts.pojo.Flight;

public class FlightDao extends Dao{

	public FlightDao() {
		
	}
	
	public Flight create(Flight flight) throws FlightException{
		try {
			begin();
			getSession().save(flight);
			commit();
			return flight;
		}catch(HibernateException ex) {
			rollback();
			throw new FlightException("Could not create flight: " + ex.getMessage(), ex.getCause());
		}finally {
			close();
		}
	}
	
	public List<Flight> getFlights(long airlineId) throws FlightException{
		try {
			begin();
			Criteria crit = getSession().createCriteria(Flight.class);
			crit.add(Restrictions.eq("id", airlineId));
			List<Flight> flights = crit.list();
			commit();
			return flights;
		}catch(HibernateException ex) {
			rollback();
			throw new FlightException("Could not create flight: " + ex.getMessage(), ex.getCause());
		}finally {
			close();
		}
	}
	
	public List<Flight> getFlights(Date date) throws FlightException{
		try {
			begin();
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			int day = cal.get(Calendar.DAY_OF_WEEK);
			Criteria crit = getSession().createCriteria(Flight.class);
			crit.add(Restrictions.eq("date", day));
			List<Flight> flights = crit.list();
			commit();
			return flights;
		}catch(HibernateException ex) {
			rollback();
			throw new FlightException("Could not create flight: " + ex.getMessage(), ex.getCause());
		}finally {
			close();
		}
	}
	
	public List<Flight> getFlights(String origin, String destination, Date date) throws FlightException{
		try {
			begin();
			Criteria crit = getSession().createCriteria(Flight.class);
			Criteria dateCrit = crit.createCriteria("dates");
			dateCrit.add(Restrictions.eq("date", date));
			crit.add(Restrictions.eq("origin", origin));
			crit.add(Restrictions.eq("destination", destination));
			List<Flight> flights = crit.list();
			commit();
			return flights;
		}catch(HibernateException ex) {
			rollback();
			throw new FlightException("Could not create flight: " + ex.getMessage(), ex.getCause());
		}finally {
			close();
		}
	}
	
	public void closeFlight(Flight flight) throws FlightException{
		try {
			begin();
			getSession().delete(flight);
			commit();			
		}catch(HibernateException ex) {
			rollback();
			throw new FlightException("Could not close Flight: "+ex.getMessage(), ex.getCause());
		}finally {
			close();
		}
	}
	
	
}
