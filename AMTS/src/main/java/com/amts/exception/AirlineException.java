package com.amts.exception;

@SuppressWarnings("serial")
public class AirlineException extends Exception{
	
	public AirlineException(String message) {
		super("User Exception: "+message);
	}
	
	public AirlineException(String message, Throwable cause) {
		super("User Exception: "+message, cause);
	}
}
