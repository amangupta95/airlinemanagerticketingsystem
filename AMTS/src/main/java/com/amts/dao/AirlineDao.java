package com.amts.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import com.amts.exception.AirlineException;
import com.amts.pojo.Airline;

public class AirlineDao extends Dao{
	
	public AirlineDao() {
		
	}
	
	public int register(Airline airline) throws AirlineException{
		
		try {
			begin();
			int i = (Integer)getSession().save(airline);
			commit();
			return i;
		}catch(HibernateException ex) {
			rollback();
			throw new AirlineException(ex.getMessage());
		}finally {
			close();
		}
	}
	
	public List<Airline> listAirlines() throws AirlineException {
		try {
			begin();
			Criteria crit = getSession().createCriteria(Airline.class);
			List<Airline> airlines = crit.list();
			commit();
			return airlines;
		}catch(HibernateException ex) {
			rollback();
			throw new AirlineException("Could not get airlines: "+ ex.getMessage());
		}finally {
			close();
		}
	}
	
	public Airline getAirline(String email, String password) throws AirlineException {
		try {
			begin();
			Criteria crit = getSession().createCriteria(Airline.class);
			crit.add(Restrictions.eq("email", email));
			crit.add(Restrictions.eq("password", password));
			Airline airline = (Airline)crit.uniqueResult();
			commit();
			return airline;
		}catch(HibernateException ex) {
			rollback();
			throw new AirlineException("Could not get airline with email: " + email + " " + ex.getMessage());
		}finally {
			close();
		}
	}
	
	public Airline getAirline(int id) throws AirlineException {
		try {
			begin();
			Criteria crit = getSession().createCriteria(Airline.class);
			crit.add(Restrictions.eq("id", id));
			Airline airline = (Airline)crit.uniqueResult();
			commit();
			return airline;
		}catch(HibernateException ex) {
			rollback();
			throw new AirlineException("Could not get airline with id: " + id + " " + ex.getMessage());
		}finally {
			close();
		}
	}
	
	public void delete(Airline airline) throws AirlineException {
		try {
			begin();
			getSession().delete(airline);
			commit();
		}catch(HibernateException ex) {
			rollback();
			throw new AirlineException("Failed to delete airline \'"+ airline +"\': "+ ex.getMessage(), ex.getCause());
		}finally {
			close();
		}
	}
	
	public void approveAirline(long airlineId) throws AirlineException{
		try {
			begin();
			Criteria crit = getSession().createCriteria(Airline.class);
			crit.add(Restrictions.eq("airlineId", airlineId));
			Airline airline = (Airline)crit.uniqueResult();
			airline.setApproved(true);
			getSession().saveOrUpdate(airline);
			commit();
		}catch(HibernateException ex) {
			rollback();
			throw new AirlineException("Failed to approve airline: "+ex.getMessage());
		}finally {
			close();
		}
	}
}
