package com.amts.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.amts.dao.CityDao;
import com.amts.exception.CityException;
import com.amts.util.CityFileParser;

@Controller
public class IndexController {
	
	@RequestMapping(value="/", method = RequestMethod.GET)
	public String loginPage(CityDao cityDao) throws CityException {
		
		if(cityDao.countCity()<=0) {
			CityFileParser cfp = new CityFileParser();
			cfp.readAndSaveCities("classpath:Airports.csv", cityDao);
		}
		
		return "login";
	}
}
