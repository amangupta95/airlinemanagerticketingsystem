package com.amts.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;

import com.amts.exception.BookingException;
import com.amts.pojo.Booking;
import com.amts.pojo.Flight;
import com.amts.pojo.User;

public class BookingDao extends Dao{
	
	public BookingDao() {
		
	}
	
	public Booking bookTicket(Booking booking) throws BookingException{
		try {
			begin();
			getSession().save(booking);
			commit();
			return booking;
		}catch(HibernateException ex) {
			throw new BookingException("Could not book ticket: "+ex.getMessage(),ex.getCause());
		}finally {
			close();
		}
	}
}
