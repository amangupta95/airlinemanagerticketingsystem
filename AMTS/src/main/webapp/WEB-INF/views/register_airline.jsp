<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h2>Welcome to AMTS</h2>
	<c:set var="contextPath" value="${pageContext.request.contextPath}" />
	<form:form action="${contextPath}/register/airline" method="post" modelAttribute="airline">
		<label>Email:    		</label><form:input path="email" type="email" required="required" placeholder="Enter your email" /><br/>
		<label>Password: 		</label><form:input path="password" type="password" required="required" placeholder="Password" /><br/>
		<label>Company Name:	</label><form:input path="name" type="text" required="required" placeholder="Company name" /><br/>
		<label>Headquarters:	</label><form:input path="headquarters" type="text" required="required" placeholder="Headquarters"/><br/>
		<label>Established Date:</label><form:input path="estDate" type="date" required="required" placeholder="Established Date"/><br/>
		<label>Capital:			</label><form:input path="capital" type="number" required="required" placeholder="Capital"/><br/>
		<input type="submit" value="Register Airline" name="submit"/>
	</form:form>
</body>
</html>