<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">  
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>  
   <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
   <script src="<c:url value="/resources/js/jquery-ui.multidatespicker.js"/>"></script> 
<link rel="stylesheet" href="<c:url value="/resources/css/main.css" />" type="text/css" />
</head>
<body style="display: flex">
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
	<div style="width: 20%; background-color: powderblue; height: 562px; text-align: center">
		<ul id="airlineMenu">
			<li><a href="${contextPath}/airline/dashboard">Dashboard</a></li>
			<li><a href="${contextPath}/aircraft/orderAircraft">Aircrafts</a></li>
			<li><a href="${contextPath}/flight/createFlight">Flights</a></li>
		</ul>
	</div>
	<div>
		<div id="flight">
			<form action="${contextPath}/flight/createFlight" method="post">
				<label>From: </label>
			<select name="fromCity">
				<c:forEach items="${cities}" var="city">
					<option value="${city.iata}">
						<div style="display: flex">
							<div style="width: 70%">
								<p>${city.cityName}</p>,<p> ${city.country}</p></br>
								<p>${city.airport}</p>
							</div>
							<div style="width: 30%">${city.iata}</div>
						</div>
					</option>
				</c:forEach>
			</select></br>
			<label>To: </label>
			<select name="toCity">
				<c:forEach items="${cities}" var="city">
					<option value="${city.iata}">
						<div style="display: flex">
							<div style="width: 70%">
								<p>${city.cityName}</p>,<p> ${city.country}</p></br>
								<p>${city.airport}</p>
							</div>
							<div style="width: 30%">${city.iata}</div>
						</div>
					</option>
				</c:forEach>
			</select><br/>
			<label>Date: </label>
			<input id = "datePick" type="text" name="departDates" min="${today}" max="${maxDate}"/><br/>
			<div>
				<label>Depart Time: </label>
				<input type="time" name="departureTime"/>
			</div>
			<div>
				<label>Aircraft: </label>
				<select name="aircraft">
					<c:forEach items="${aircrafts}" var="aircraft">
						<option value="${aircraft.model}">${aircraft.model}</option>
					</c:forEach>
				</select>
			</div>
			<div>
				<label>Fare: $</label>
				<input type="number" min=0 name="price"/>
			</div>
			<input type="submit" value="Create Flight" name="submit"/>
			</form>
		</div>
		<div id="displayFlights">
			<table id="flight_table">
				<thead>
					<tr>
						<th>Flight_ID</th>
						<th>Origin</th>
						<th>Destination</th>
						<th>Aircraft</th>
						<th>Date</th>
						<th>Fare</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${flights}" var="flight">
						<tr>
							<td>${flight.id}</td>
							<td>${flight.origin}</td>
							<td>${flight.destination}</td>
							<td list="dates">
								<datalist id="dates">
									<c:forEach items="${flight.date}" var="date">
										<option value="${date}">
									</c:forEach>
								</datalist>
							</td>
							<td>${flight.date}</td>
							<td>${flight.fare}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	<script>
	$(document).ready(function(){  
	    $('#datePick').multiDatesPicker({
	    	minDate: 0,
	    	maxDate: 90
	    });  
	});  
	</script>
</body>
</html>