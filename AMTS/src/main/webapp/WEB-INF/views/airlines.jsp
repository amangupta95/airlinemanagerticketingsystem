<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="<c:url value="/resources/css/main.css" />" type="text/css" />
<title>Insert title here</title>
</head>
<body style="display: flex">
	<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
	<div style="width: 20%; background-color: powderblue; height: 562px; text-align: center">
		<ul id="adminMenu">
			<li><a href="${contextPath}/admin/dashboard">Dashboard</a></li>
			<li><a href="${contextPath}/aircraft/addAircraft">Aircrafts</a></li>
			<li><a href="${contextPath}/airline/approveAirline">Airlines</a></li>
		</ul>
	</div>

</body>
</html>