 package com.amts.pojo;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity(name="Flight")
public class Flight {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@OneToOne
	private City originCity;
	
	@OneToOne
	private City destinationCity;
	
	@Column(name="distance")
	private int distance;
	
	@Column(name="departure")
	private Date departTime;
	
	@Column(name="arrival")
	private Date arrivalTime;
	
	@ManyToMany(cascade=CascadeType.ALL, mappedBy="flights")
	private Set<DateValue> dates;
	
	@OneToMany(mappedBy="flight")
	private Set<Booking> bookings;
	
	@ManyToOne
	@JoinColumn(name="airline_id", nullable=false)
	private Airline airline;
	
	@Column(name="fare")
	private double price;
	
	
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public Airline getAirline() {
		return airline;
	}
	public void setAirline(Airline airline) {
		this.airline = airline;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public City getOriginCity() {
		return originCity;
	}
	public void setOriginCity(City originCity) {
		this.originCity = originCity;
	}
	public City getDestinationCity() {
		return destinationCity;
	}
	public void setDestinationCity(City destinationCity) {
		this.destinationCity = destinationCity;
	}
	public int getDistance() {
		return distance;
	}
	public void setDistance(int distance) {
		this.distance = distance;
	}
	public Date getDepartTime() {
		return departTime;
	}
	public void setDepartTime(Date departTime) {
		this.departTime = departTime;
	}
	public Date getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(Date arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public Set<DateValue> getDates() {
		return dates;
	}
	public void setDates(Set<DateValue> dates) {
		this.dates = dates;
	}
}
