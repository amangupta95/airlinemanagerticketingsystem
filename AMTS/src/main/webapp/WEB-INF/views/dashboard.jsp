<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="<c:url value="/resources/css/main.css" />" type="text/css" />
</head>
<body style="display: flex">
	<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
	<div style="width: 20%; background-color: powderblue; height: 562px; text-align: center">
		<ul id="airlineMenu">
			<li><a href="${contextPath}/airline/dashboard">Dashboard</a></li>
			<li><a href="${contextPath}/aircraft/orderAircraft">Aircrafts</a></li>
			<li><a href="${contextPath}/flight/createFlight">Flights</a></li>
		</ul>
	</div>
	<div style="width: 80%">
		<div id="dashboard">
			<table id="stats_table">
				<tr><td>Total aircrafts: </td><td>${aircrafts}</td></tr>
				<tr><td>Total flights: </td><td>${flights}</td></tr>
				<tr><td>Total crew: </td><td>${crew}</td></tr>
				<tr><td>Revenue generated: </td><td>${revenue}</td></tr>
			</table>
		</div>
	</div>
</body>
</html>