package com.amts.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import com.amts.exception.AircraftException;
import com.amts.pojo.Aircraft;
import com.amts.pojo.Airline;

public class AircraftDao extends Dao{
	
	public AircraftDao() {
		
	}
	
	public Aircraft add(Aircraft aircraft, int airline_id) throws AircraftException{
		try {
			begin();
			Criteria crit = getSession().createCriteria(Airline.class);
			crit.add(Restrictions.eq("id", airline_id));
			Airline airline = (Airline)crit.uniqueResult();
			crit = getSession().createCriteria(Aircraft.class);
			crit.add(Restrictions.eq("airline", airline));
			crit.add(Restrictions.eq("model", aircraft.getModel()));
			if(crit.uniqueResult()!=null) {
				Aircraft air = (Aircraft)crit.uniqueResult();
				double count = air.getCount();
				air.setCount(count+aircraft.getCount());
				getSession().update(air);
			}else {
				getSession().save(aircraft);
			}
			airline.setCapital(airline.getCapital()-(aircraft.getCount()*aircraft.getPrice()));
			getSession().update(airline);
			commit();
			return aircraft;
		}catch(HibernateException ex) {
			rollback();
			throw new AircraftException("Could not save aircraft: "+ex.getMessage(), ex.getCause());
		}finally {
			close();
		}
	}
	//get all aircrafts from the database and display to admin.
	public List<Aircraft> listAll() throws AircraftException{
		try {
			begin();
			Criteria crit = getSession().createCriteria(Aircraft.class);
			List<Aircraft> aircrafts = crit.list();
			commit();
			return aircrafts;
		}catch(HibernateException ex) {
			rollback();
			throw new AircraftException("Could not save aircraft: "+ex.getMessage(), ex.getCause());
		}finally {
			close();
		}
	}
	
//	public List<Aircraft> getAirlineAircrafts(long airlineId) throws AircraftException{
//		try {
//			begin();
//			Criteria crit = getSession().createCriteria(Airline.class);
//			crit.add(Restrictions.eq(propertyName, value))
//			List<Aircraft> aircrafts = crit.list();
//			commit();
//			return aircrafts;
//		}catch(HibernateException ex) {
//			rollback();
//			throw new AircraftException("Could not save aircraft: "+ex.getMessage(), ex.getCause());
//		}finally {
//			close();
//		}
//	}
	
}
