package com.amts.exception;

public class FlightException extends Exception {
	
	public FlightException(String message) {
		super("Flight Exception: " + message);
	}
	
	public FlightException(String message, Throwable cause) {
		super("Flight Exception: " + message, cause);
	}
}
