package com.amts.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

import com.amts.exception.AircraftRequestException;
import com.amts.pojo.AircraftRequest;

public class AircraftOrderDao extends Dao{
	
	public void submitOrder(AircraftRequest request) throws AircraftRequestException{
		try {
			begin();
			getSession().save(request);
			commit();
		}catch(HibernateException ex) {
			throw new AircraftRequestException(ex.getMessage(), ex.getCause());
		}
	}
	
	public List<AircraftRequest> getOrders() throws AircraftRequestException {
		List<AircraftRequest> requests = null;
		try {
			begin();
			Criteria crit = getSession().createCriteria(AircraftRequest.class);
			crit.add(Restrictions.eq("status", false));
			requests = (List<AircraftRequest>)crit.list();
			commit();
			return requests;
		}catch(HibernateException ex) {
			throw new AircraftRequestException(ex.getMessage(), ex.getCause());
		}
	}
	
	public void updateOrder(int id) throws AircraftRequestException{
		try {
			begin();
			Criteria crit = getSession().createCriteria(AircraftRequest.class);
			crit.add(Restrictions.eq("id", id));
			AircraftRequest request = (AircraftRequest)crit.uniqueResult();
			request.setStatus(true);
			getSession().update(request);
			commit();
		}catch(HibernateException ex) {
			throw new AircraftRequestException(ex.getMessage(),ex.getCause());
		}
	}
}
