package com.amts.exception;

public class AircraftRequestException extends Exception {
	public AircraftRequestException(String message, Throwable cause) {
		super(message, cause);
	}
}
