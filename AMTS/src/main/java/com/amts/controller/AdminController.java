package com.amts.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.amts.dao.AirlineDao;
import com.amts.dao.UserDao;

@Controller
@RequestMapping("/admin/*")
public class AdminController {

	@RequestMapping(value="/dashboard", method=RequestMethod.GET)
	public String getDashboard(ModelMap modelMap, UserDao userDao, AirlineDao airlineDao) {
		
		return "dashboard";
	}
}
