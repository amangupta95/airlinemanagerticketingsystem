package com.amts.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.springframework.util.ResourceUtils;

import com.amts.dao.CityDao;
import com.amts.exception.CityException;
import com.amts.pojo.City;

public class CityFileParser {
	
	public void readAndSaveCities(String fileName, CityDao cityDao) throws CityException{
		String csvSplitBy=",";
		BufferedReader br;
		try
		{
			File file = ResourceUtils.getFile(fileName);
			br = new BufferedReader(new FileReader(file));
			while(br.readLine()!=null) {
				
				String[] city = br.readLine().split(csvSplitBy);
				City c = new City();
				c.setAirport(city[0]);
				c.setCityName(city[1]);
				c.setCountry(city[2]);
				c.setIata(city[3]);
				c.setLatitude(Double.parseDouble(city[4]));
				c.setLongitude(Double.parseDouble(city[5]));
				cityDao.populateCity(c);
			}
			br.close();
		}catch(IOException ex) {
			ex.printStackTrace();
		}
	}
}
