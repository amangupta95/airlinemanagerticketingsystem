<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="<c:url value="/resources/css/main.css" />" type="text/css" />
<title>Insert title here</title>
</head>
<body style="display: flex">
	<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
	<div style="width: 20%; background-color: powderblue; height: 562px; text-align: center">
		<ul id="adminMenu">
			<li><a href="${contextPath}/admin/dashboard">Dashboard</a></li>
			<li><a href="${contextPath}/aircraft/addAircraft">Aircrafts</a></li>
			<li><a href="${contextPath}/airline/approveAirline">Airlines</a></li>
		</ul>
	</div>
	<div style="width: 80%">
		<div id="aircraft">
			<table id="stats_table">
				<thead>
					<tr>
						<th>Airline</th>
						<th>Aircraft Model</th>
						<th>Aircraft Manufacturer</th>
						<th>Capacity</th>
						<th>No. of Aircrafts</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${orderRequests}" var="item">
						<tr>
							<form:form action="${contextPath}/aircraft/addAircraft" method="post"
							modelAttribute="aircraft">
								<td>
									<span>${item.value.airline.name}</span>
									<input type="hidden" name="airline_id" value="${item.value.airline.id}" />
									<input type="hidden" name="request_id" value="${item.key}"/>
									<form:input type="hidden" path="price" required="required" value="${item.value.price}"/>
								</td>
								<td>
									<form:input type="hidden" path="model" required="required" value="${item.value.model}"/><span>${item.value.model}</span>
								</td>
								<td>
									<form:input type="hidden" path="manufacturer" required="required"  value="${item.value.manufacturer}"/><span>${item.value.manufacturer}</span>
								</td>
								<td>
									<form:input type="hidden" path="capacity" min="4" max="600" required="required" value="${item.value.capacity}"/><span>${item.value.capacity}</span>
								</td>
								<td>
									<form:input type="hidden" path="count" required="required" value="${item.value.count}"/><span>${item.value.count}</span>
								</td>
								<td>
									<input type="submit" name="submit" value="Confirm Order" />
								</td>
							</form:form>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			
		</div>
	</div>
</body>
</html>