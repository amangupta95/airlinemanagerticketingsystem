package com.amts.controller;

import javax.servlet.http.HttpServletRequest;
import javax.swing.text.html.FormSubmitEvent.MethodType;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.amts.dao.AirlineDao;

@Controller
@RequestMapping("/airline/*")
public class AirlineController {

	@RequestMapping(value="/dashboard", method=RequestMethod.GET)
	public String displayDashboard() {
		return "dashboard";
	}
	
	@RequestMapping(value="/approveAirline", method=RequestMethod.GET)
	public String approveAirlineView() {
		return "airlines";
	}
	
	@RequestMapping(value="/approveAirline", method=RequestMethod.POST)
	public void approveAirline(AirlineDao airlineDao, HttpServletRequest request) {
		approveAirlineView();
	}
	
//	@RequestMapping(value="/createFlight", method=RequestMethod.GET)
//	public String displayFlights(ModelMap modelMap) {
//		
//		return "createFlight";
//	}
//	
//	@RequestMapping(value="/orderAircraft", method=RequestMethod.GET)
//	public String displayAircrafts() {
//		return "orderAircraft";
//	}
}
