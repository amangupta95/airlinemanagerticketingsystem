package com.amts.exception;

public class AircraftException extends Exception {
	
		public AircraftException(String message) {
			super("Aircraft Exception: " + message);
		}
		
		public AircraftException(String message, Throwable cause) {
			super("Aircraft Exception: " + message, cause);
		}			
}
